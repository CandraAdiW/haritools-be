FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN apt-get update && apt-get install -y --no-install-recommends \
    libgl1 \
    libglib2.0-0 \
    ghostscript 

COPY ./requirements.txt /app/requirements.txt

RUN /usr/local/bin/python -m pip install --upgrade pip

RUN pip install --upgrade -r /app/requirements.txt

COPY ./ /app/
