from prosesFile import fileHandler,pdfHandler,pcapHandler,docHandler,cleanHandler,imageHandler
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.staticfiles import StaticFiles
from fastapi_utils.tasks import repeat_every

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
    "http://localhost:4200",
    "https://haritools.com"
]


app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.on_event("startup")
@repeat_every(seconds=60*60) #setiap jam bersih bersih
def removeOldFile():
    cleanHandler.hapusFileFolderTua()

app.mount("/fileProses", StaticFiles(directory="fileProses"), name="fileProses")
app.include_router(fileHandler.router)
app.include_router(pdfHandler.router)
app.include_router(pcapHandler.router)
app.include_router(docHandler.router)
app.include_router(imageHandler.router)
