from gc import garbage
from fastapi import APIRouter, Header
from fastapi.responses import FileResponse
from pydantic import BaseModel
import string
import random
import os
from PIL import Image
from PyPDF2 import PdfFileReader, PdfFileWriter
import fitz  # PyMuPDF
import zipfile 
import shutil
from pdf2docx import parse
import subprocess

class ItemDownload(BaseModel):
    fileList: list = []
    type: str

router = APIRouter()


@router.post("/download")
async def download(dataPost: ItemDownload):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    error: bool = False
    try:
        fullPath = os.getcwd()
        tmpProses = []
        hasilProses = ''
        if(dataPost.type == 'pdf'):
            for itemPath in dataPost.fileList:
                if itemPath.split(".")[-1] == 'pdf':
                    tmpProses.append(itemPath)
                else:
                    tmp = await imageToPdf(itemPath)
                    tmpProses.append(tmp[0])
        elif(dataPost.type == 'image' or dataPost.type == 'doc'):
            #jgn ubah tipe extension biarkan original
            for itemPath in dataPost.fileList:
                tmpProses.append(itemPath)
        hasilProses = fullPath + '/' + await zipFileProses(tmpProses)
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
            return data
        else:
            return FileResponse(hasilProses)

@router.get("/mime-list")
async def mimeList():
    data = {
        "data": await dataMimeList(),
        "status": True,
        "message": ""
    }
    return data

async def randomName():
    alphabet = string.ascii_letters
    panjang = 12
    return ''.join(random.choice(alphabet) for x in range(panjang))

async def uploadFunc(fileUpload : any,mimeFile : str):
    kirimHasil = []
    mimeList = await dataMimeList()
    indexMime = [i for i, item in enumerate(mimeList) if mimeFile in item.values()]
    path = "fileUpload/"
    for itemFile in fileUpload:
        await validateFile(indexMime, mimeList, itemFile)
        tipeFile = itemFile.filename.split('.')[-1]
        pathByExtension = path+tipeFile
        if not os.path.isdir(path):
            os.mkdir(path)
        if not os.path.isdir(pathByExtension):
            os.mkdir(pathByExtension)
        random = await randomName()
        pathKirim = {
            "process": "/"+pathByExtension+"/"+random+"."+tipeFile,
            "originalname": itemFile.filename
        }
        simpanByFullpath = os.getcwd()+pathKirim["process"]
        with open(simpanByFullpath, 'wb') as f:
            [f.write(chunk) for chunk in iter(
                lambda: itemFile.file.read(10000), b'')]
        kirimHasil.append(pathKirim)
    return kirimHasil

# 10MB
async def valid_content_length(content_length: int = Header(..., lt=100_000_000)):
    return content_length

async def validateFile(indexMime, mimeList, itemFile):
    print()
    if len(indexMime) > 0:
        if itemFile.content_type not in mimeList[indexMime[0]]["mime"]:
            #jika mime tidak cocok
            raise Exception("Mime File not match")
    else:
        raise Exception("Mime File not found")

async def pdfSplit(pathFile: str):
    hasilProsess = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()
    if not os.path.isdir(path): #check path 
        os.mkdir(path)
    pdfFile = PdfFileReader(pathFile)
    for page in range(pdfFile.getNumPages()):
        pdf_writer = PdfFileWriter()
        pdf_writer.addPage(pdfFile.getPage(page))

        output_filename = '/'+path+f'split_page_{page+1}.pdf'

        with open(fullPath+output_filename, 'wb') as out:
            pdf_writer.write(out)
            hasilProsess.append(output_filename)
    pdfFile.close()
    return hasilProsess

async def pdfToImage(pathFile: str):
    hasilProses = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    doc = fitz.open(pathFile)
    for halaman in range(doc.pageCount):
        page = doc[halaman]
        rotate = int(0)
        zoom_x = 2
        zoom_y = 2
        mat = fitz.Matrix(zoom_x, zoom_y).prerotate(rotate)
        pix = page.get_pixmap(matrix=mat, alpha=False)
        output_file = '/'+path + \
            f'split_page_{halaman+1}.png'
        pix.save(fullPath+output_file)
        hasilProses.append(output_file)
    doc.close()
    return hasilProses

async def imageToPdf(pathFile : str):
    fullPath    = os.getcwd()
    tmpNama = pathFile.split(".png")
    hasilProses = tmpNama[0]+'.pdf'
    pathSimpan = fullPath+'/'+hasilProses
    dataGambar = Image.open(fullPath+'/'+pathFile)
    tmpDataGambar = dataGambar.convert('RGB')
    tmpDataGambar.save(pathSimpan)
    dataGambar.close()
    return hasilProses

async def zipFileProses(fileList : list):
    fullPath = os.getcwd()
    randomString = await randomName()
    namaZip = ["fileProses",
               fileList[0].replace("fileProses/", "").split("/")[0]]
    namaZip.append(randomString+".zip")
    namaZip = "/".join(namaZip)
    hasilProses = namaZip
    zipObject = zipfile.ZipFile(
        fullPath+'/'+namaZip, 'w', zipfile.ZIP_DEFLATED)
    for itemFile in fileList:
        tmpArcname = itemFile.split("/")
        print(tmpArcname)
        print(tmpArcname[len(tmpArcname)-1])
        zipObject.write(filename=fullPath+'/'+itemFile,
                        arcname=tmpArcname[len(tmpArcname)-1])
    zipObject.close()
    return hasilProses

async def extractImageDocs(pathFile: str):
    fullPath = os.getcwd()
    randomString = await randomName()
    path = "fileProses/"+randomString+"/"
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    copyPath = fullPath+"/"+path+randomString+".zip"
    extractPath = copyPath.split(".zip")[0]
    shutil.copyfile(pathFile, copyPath)  # copy dan rename extension
    with zipfile.ZipFile(copyPath,'r') as zipObj:
        zipObj.extractall(extractPath)
    pathFileImage = extractPath+'/word/media'
    listFileImage = os.listdir(pathFileImage)
    fileProsesPath = pathFileImage.replace(fullPath,"")
    hasilProses = list(map(lambda xpath: fileProsesPath +"/"+ xpath, listFileImage))
    return hasilProses

async def extractImagePdf(pathFile:str):
    hasil = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()+"/"+path
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    doc = fitz.open(pathFile)
    for halaman in range(doc.pageCount):
        imageList = doc.get_page_images(halaman)
        for imageIndex in imageList:
            xref = imageIndex[0]
            pix = fitz.Pixmap(doc,xref)
            if pix.n < 5:
                pix._writeIMG(fullPath+f'{xref}.png', 1)
                hasil.append("/"+path+f'{xref}.png')
    doc.close()
    return hasil

async def imageToPdf(pathFile : str):
    hasil = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()+"/"+path
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    namaOuput = fullPath+''+await randomName()+".pdf"
    namaRetun = namaOuput.replace(fullPath,path)
    objGambar = Image.open(pathFile)
    tmpObjGambar = objGambar.convert('RGB')
    tmpObjGambar.save(namaOuput)
    objGambar.close()
    hasil.append(namaRetun)
    return hasil

async def pdfToDoc(pathFile : str):
    hasil = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()+"/"+path
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    namaOuput = fullPath+''+await randomName()+".docx"
    namaRetun = namaOuput.replace(fullPath, path)
    parse(pathFile, namaOuput)
    hasil.append(namaRetun)
    return hasil

async def imageCompress(pathFile : str):
    hasil = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()+"/"+path
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    namaOuput = fullPath+''+await randomName()+".jpeg"
    namaRetun = namaOuput.replace(fullPath, path)
    im = Image.open(pathFile)
    im.save(namaOuput, 'JPEG',quality=9,optimize=True)
    im.close()
    hasil.append(namaRetun)
    return hasil

async def pdfCompress(pathFile : str):
    hasil = []
    path = "fileProses/"+await randomName()+"/"
    fullPath = os.getcwd()+"/"+path
    if not os.path.isdir(path):  # check path
        os.mkdir(path)
    namaOuput = fullPath+''+await randomName()+".pdf"
    namaRetun = namaOuput.replace(fullPath, path)
    subprocess.call(['gs', '-sDEVICE=pdfwrite', '-dCompatibilityLevel=1.4',
                     '-dPDFSETTINGS=/default',
                     '-dNOPAUSE', '-dQUIET', '-dBATCH',
                     '-sOutputFile={}'.format(namaOuput),
                     pathFile]
                    )
    hasil.append(namaRetun)
    return hasil   

async def imageMeta(pathFile : str):
    hasil = []
    harilProses = subprocess.check_output(['identify', '-verbose', pathFile])
    tmp = harilProses.decode("utf-8").split("\n")
    lambda: [item.strip() for item in tmp]
    for item in tmp:
        if 'Image:' not in item and 'Artifacts:' not in item and 'filename:' not in item and 'verbose:' not in item and 'Version:' not in item:
            hasil.append(item)
    return hasil

async def docToPdf(pathFile : str):
    hasil = []
    # path = "fileProses/"+await randomName()+"/"
    # fullPath = os.getcwd()+"/"+path
    # if not os.path.isdir(path):  # check path
    #     os.mkdir(path)
    # #with fitz.open(pathFile) as doc:
    # print(f'pathFile {pathFile}')
    # print(f'asdasdasdsadsa {fitz.VersionBind}')
    # namaOuput = fullPath+'/'+await randomName()+"/"+".pdf"
    # print(f'namaOuputnamaOuputnamaOuput {namaOuput}')
    # convert(pathFile, namaOuput)
    return hasil



async def dataMimeList():
    return [
        {
            "type" : "pcap",
            "mime" : ["application/vnd.tcpdump.pcap"]
        },
        {
            "type": "doc",
            "mime": ["application/wps-office.docx","application/vnd.openxmlformats-officedocument.wordprocessingml.document", "application/msword"]
        },
        {
            "type": "ppt",
            "mime": ["application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation"]
        },
        {
            "type": "xls",
            "mime": ["application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"]
        },
        {
            "type": "pdf",
            "mime": ["application/pdf"]
        },
        {
            "type": "image",
            "mime": ["image/jpeg", "image/png"]
        },
        {
            "type": "size",
            "data": 10
        }
    ]
