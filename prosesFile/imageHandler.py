from fastapi import APIRouter, UploadFile, File, Depends
from prosesFile import fileHandler
from typing import List
import os


router = APIRouter()

@router.post("/image-to-pdf", dependencies=[Depends(fileHandler.valid_content_length)])
async def imageToPdf(file: List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'image')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.imageToPdf(os.getcwd()+hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data

@router.post("/image-compress",dependencies=[Depends(fileHandler.valid_content_length)])
async def imageCompress(file : List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'image')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.imageCompress(os.getcwd()+hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data

@router.post("/image-metadata",dependencies=[Depends(fileHandler.valid_content_length)])
async def imageMeta(file : List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'image')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.imageMeta(os.getcwd()+hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data
