import os
import time
import shutil

def hapusFileFolderTua():
    waktuSekarang = time.time()
    cekHapusFileProses(waktuSekarang)
    cekHapusFileUpload(waktuSekarang)


def cekHapusFileProses(waktuSekarang):
    folderProses = os.path.join(os.getcwd(), "fileProses")
    listDir = os.listdir(folderProses)
    for itemDir in listDir:
        pathFullDir = os.path.join(folderProses, itemDir)
        if os.path.isdir(pathFullDir):
            waktuObj = os.stat(pathFullDir)
            waktuDir = waktuObj.st_mtime
            selisihWaktu = waktuSekarang - waktuDir
            if selisihWaktu//60 > 60:
                print(
                    f'File / Folder {itemDir} -- {waktuDir//60} Selisih {selisihWaktu//60}')
                print("hapus")
                shutil.rmtree(pathFullDir)


def cekHapusFileUpload(waktuSekarang):
    folderUpload = os.path.join(os.getcwd(), "fileUpload")
    print(folderUpload)
    listDir = os.listdir(folderUpload)
    for itemDir in listDir:
        listInsideDir = os.listdir(os.path.join(folderUpload,itemDir))
        for itemInsideDir in listInsideDir:
            fullPath = os.path.join(folderUpload, itemDir, itemInsideDir)
            waktuObj = os.stat(fullPath)
            waktuDir = waktuObj.st_mtime
            selisihWaktu = waktuSekarang - waktuDir
            if selisihWaktu//60 > 60:
                print(
                    f'File / Folder {itemInsideDir} -- {waktuDir//60} Selisih {selisihWaktu//60}')
                print("hapus")
                os.remove(fullPath)
