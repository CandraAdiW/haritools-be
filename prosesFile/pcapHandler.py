from fastapi import APIRouter, UploadFile, File
from prosesFile import fileHandler
from typing import List
from fastapi import Request
import os
from scapy.all import *
from sse_starlette.sse import EventSourceResponse
import asyncio
import json

router = APIRouter()

@router.get("/pcap-process")
async def pcapToJson(request: Request):  # file: List[UploadFile] = File(...)
    return EventSourceResponse(loopPcap(request), media_type="text/event-stream")


async def loopPcap(request):
    dataKirim = {
        "protokol": "",
        "sip"  : "",
        "dip"  : "",
        "sipV6": "",
        "dipV6": "",
        "sport": "",
        "dport": "",
        "tcpFlags" : ""
    }
    for dataPacket in PcapReader("/home/candra/Documents/Experimen/Angular/test-ssr/testData/test.pcapng"):
        #print(dataPacket.show())
        if await request.is_disconnected():
            print("client disconnected!!!")
            break
        if(dataPacket.haslayer(IPv6)):
            dataKirim["dipV6"] = dataPacket[IPv6].dst
            dataKirim["sipV6"] = dataPacket[IPv6].src
        if(dataPacket.haslayer(IP)):
            dataKirim["dip"] = dataPacket[IP].dst
            dataKirim["sip"] = dataPacket[IP].src
        if(dataPacket.haslayer(UDP)):
            dataKirim["protokol"] = "UDP"
            dataKirim["dport"]    = dataPacket[UDP].dport
            dataKirim["sport"]    = dataPacket[UDP].sport
        elif (dataPacket.haslayer(TCP)):
            dataKirim["protokol"] = "TCP"
            dataKirim["dport"] = dataPacket[TCP].dport
            dataKirim["sport"] = dataPacket[TCP].sport
            dataKirim["tcpFlags"] = str(dataPacket[TCP].flags)
        print(dataKirim)
        yield json.dumps(dataKirim)
        await asyncio.sleep(1)

