from fastapi import APIRouter, UploadFile, File, Depends
from prosesFile import fileHandler
from typing import List
import os

router = APIRouter()

@router.post("/split-pdf", dependencies=[Depends(fileHandler.valid_content_length)])
async def pdfSplit(file: List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim  = []
    error : bool  = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'pdf')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                # multi halaman pdf jadi multi image split
                "process": await fileHandler.pdfToImage(os.getcwd()+hasilUpload["process"]),
                "originalname"  : hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"]  = True
            data["data"]    = pathKirim
        return data

@router.post("/extract-image-pdf", dependencies=[Depends(fileHandler.valid_content_length)])
async def pdfExtractImage(file :List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim  = []
    error : bool  = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'pdf')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.extractImagePdf(os.getcwd()+ hasilUpload["process"]),
                "originalname"  : hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"]  = True
            data["data"]    = pathKirim
        return data

@router.post("/pdf-compress",dependencies=[Depends(fileHandler.valid_content_length)])
async def pdfCompress(file : List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'pdf')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.pdfCompress(os.getcwd() + hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data


@router.post("/pdf-to-doc",dependencies=[Depends(fileHandler.valid_content_length)])
async def pdfToDoc(file : List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'pdf')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                "process": await fileHandler.pdfToDoc(os.getcwd() + hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data
