from fastapi import APIRouter, UploadFile, File,Depends
from prosesFile import fileHandler
from typing import List
import os


router = APIRouter()


@router.post("/doc-extract-image", dependencies=[Depends(fileHandler.valid_content_length)])
async def extractImage(file: List[UploadFile] = File(...)):
    data = {
        "data": [],
        "status": False,
        "message": ""
    }
    pathKirim = []
    error: bool = False
    try:
        hasilUpload = await fileHandler.uploadFunc(file, 'doc')
        for hasilUpload in hasilUpload:
            pathKirim.append({
                # multi halaman pdf jadi multi image split
                "process": await fileHandler.extractImageDocs(os.getcwd()+hasilUpload["process"]),
                "originalname": hasilUpload["originalname"]
            })
    except Exception as e:
        error = True
        print(e)
    finally:
        if error:
            data["message"] = "Process Failed"
        else:
            data["message"] = "Process Success"
            data["status"] = True
            data["data"] = pathKirim
        return data
